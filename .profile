# ~/.profile: executed by the command interpreter for login shells.

export PATH="$HOME/bin:$PATH"

export PLAN9="/usr/local/plan9port"
export PATH="$PATH:$PLAN9/bin"

export GOROOT=/usr/local/go
export PATH=$PATH:$GOROOT/bin
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

export font='/mnt/font/Go Mono/11a/font'
export acmeshell='sh'

export JAVA_HOME="/usr/local/java/jdk1.8.0_144"
export PATH="$PATH:$JAVA_HOME/bin"

export XAUTHORITY="$HOME/.Xauthority"

export BROWSER='firefox'
