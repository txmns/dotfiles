#!/bin/sh
#xrandr --output VGA-0 --off --output LVDS --primary --mode 1366x768 --pos 0x312 --rotate normal --output HDMI-0 --mode 1920x1080 --pos 1366x0 --rotate normal &
xrandr --output VGA-0 --mode 1920x1080 --pos 1366x0 --rotate normal --output LVDS --primary --mode 1366x768 --pos 0x152 --rotate normal --output HDMI-0 --off &
fontsrv &
plumber &
xsetroot -solid '#006699' &
nm-applet &
numlockx on &
