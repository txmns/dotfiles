call plug#begin('~/.local/share/nvim/plugged')

Plug 'fatih/vim-go'
Plug 'ctrlpvim/ctrlp.vim'

call plug#end()

set autowrite

syntax off
"filetype off

" All error lists are the 'quickfix' type.
let g:go_list_type = "quickfix"

" Replace 'gofmt' with 'goimports'
let g:go_fmt_command = "goimports"

let mapleader = ","

map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>

autocmd FileType go nmap <leader>b <Plug>(go-build)
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>t <Plug>(go-test)
autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)

